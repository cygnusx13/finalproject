package UserWindow;

import ServerUniversitas.*;
//import ServerUniversitas.Questions;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Wilson Castro. 2017
 */
public class ExamInterface extends javax.swing.JFrame {

    int posicion = 0;
    String text = null;
    String questi = null; 
    String textAndQuestion = null;
    Questions quest = new Questions();
    Answers answ = new Answers();
    Object[] select = {"", "", "", "", "", "", "", "", ""};

    public ExamInterface() throws IOException, ClassNotFoundException, SQLException {
        initComponents();

        textAndQuestion = quest.getTextQuestionsDB(0);
        
        if (getText(textAndQuestion).equals(" ")) {
            jTextField1.setText(getQuestion(textAndQuestion));
        } else {
            jTextField1.setText(getText(textAndQuestion) + "\n\n"
                    + getQuestion(textAndQuestion));
        }

        String[] a = answ.setAnswers(posicion);
        buttonGroup1.clearSelection();
        opc1.setText(a[0]);
        opc2.setText(a[1]);
        opc3.setText(a[2]);
        opc4.setText(a[3]);
        opc1.requestFocus();
        regresar.setEnabled(false);
        terminar.setEnabled(false);
        //btExit.setEnabled(false);
    }

    public void toSepare(String texto) {
        String tex = texto;
        StringTokenizer token = new StringTokenizer(tex, "|");

        while (token.hasMoreTokens()) {
            text = token.nextToken();
            questi = token.nextToken();
        }
    }

    public String getText(String texto) {
        toSepare(texto);
        return text;
    }

    public String getQuestion(String texto) {
        toSepare(texto);
        return questi;
    }

    /*   public String setRespuestas(String sttrin) {
        //String s1 = radioR[posicion];
        String[] s2 = toSepare(sttrin, "/n");
        return s2;
    } */

 /*   public Image getIconImage(){
        Image retValue= Toolkit.getDefaultToolkit().getImage
            (ClassLoader.getSystemResource("clases/universitas22.png"));
        return retValue;
    } 
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        opc1 = new javax.swing.JRadioButton();
        opc2 = new javax.swing.JRadioButton();
        opc3 = new javax.swing.JRadioButton();
        opc4 = new javax.swing.JRadioButton();
        terminar = new javax.swing.JButton();
        avanzar = new javax.swing.JButton();
        regresar = new javax.swing.JButton();
        btExit = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextField1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(1, 50, 67));

        jLabel1.setFont(new java.awt.Font("Bradley Hand ITC", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(236, 240, 241));
        jLabel1.setText("Programa para la evaluacion de conocimientos Universitas");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(157, 157, 157)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 651, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(172, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 980, 80));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setName("Examen"); // NOI18N

        buttonGroup1.add(opc1);
        opc1.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        opc1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(opc2);
        opc2.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        opc2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc2ActionPerformed(evt);
            }
        });

        buttonGroup1.add(opc3);
        opc3.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        opc3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc3ActionPerformed(evt);
            }
        });

        buttonGroup1.add(opc4);
        opc4.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        opc4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc4ActionPerformed(evt);
            }
        });

        terminar.setText("Terminar examen");
        terminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terminarActionPerformed(evt);
            }
        });

        avanzar.setText("Avanzar >>");
        avanzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avanzarActionPerformed(evt);
            }
        });

        regresar.setText("<< Regresar ");
        regresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regresarActionPerformed(evt);
            }
        });

        btExit.setText("Salir");
        btExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExitActionPerformed(evt);
            }
        });

        jTextField1.setEditable(false);
        jTextField1.setColumns(20);
        jTextField1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jTextField1.setLineWrap(true);
        jTextField1.setRows(5);
        jTextField1.setToolTipText("");
        jTextField1.setWrapStyleWord(true);
        jScrollPane1.setViewportView(jTextField1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(opc1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opc2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opc3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opc4, javax.swing.GroupLayout.Alignment.LEADING))
                        .addContainerGap(482, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(terminar)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(regresar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(avanzar)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 207, Short.MAX_VALUE)
                                .addComponent(btExit, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(14, 14, 14))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(opc1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(opc2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(opc3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(opc4)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(avanzar)
                            .addComponent(regresar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(terminar)
                        .addGap(27, 27, 27))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btExit)
                        .addContainerGap())))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-4, 77, 520, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void opc1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc1ActionPerformed
        select[posicion] = opc1.getLabel();
    }//GEN-LAST:event_opc1ActionPerformed

    private void opc2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc2ActionPerformed
        select[posicion] = opc2.getLabel();
    }//GEN-LAST:event_opc2ActionPerformed

    private void opc3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc3ActionPerformed
        select[posicion] = opc3.getLabel();
    }//GEN-LAST:event_opc3ActionPerformed

    private void opc4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc4ActionPerformed
        select[posicion] = opc4.getLabel();
    }//GEN-LAST:event_opc4ActionPerformed

    private void avanzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avanzarActionPerformed
        if (posicion == quest.quantityRowsDB() - 3) {
            avanzar.setEnabled(false);
            terminar.setEnabled(true);
            //btExit.setEnabled(true);
        }
        if (posicion <= quest.quantityRowsDB()) {
            regresar.setEnabled(true);
            btExit.setEnabled(true);
            posicion++;
            try {
                textAndQuestion = quest.getTextQuestionsDB(posicion);
            } catch (IOException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (getText(textAndQuestion).equals(" ")) {
                jTextField1.setText(getQuestion(textAndQuestion));
            } else {
                jTextField1.setText(getText(textAndQuestion) + "\n\n"
                        + getQuestion(textAndQuestion));
            }

            String[] a = answ.setAnswers(posicion);
            buttonGroup1.clearSelection();
            opc1.setText(a[0]);
            opc2.setText(a[1]);
            opc3.setText(a[2]);
            opc4.setText(a[3]);
            opc1.requestFocus();
        }
        // System.out.println(quest.quantityRowsDB());
    }//GEN-LAST:event_avanzarActionPerformed

    int score = 0;

    private void terminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terminarActionPerformed

        /*    for (int i = 0; i < 10; i++) { // Calculo de puntaje!!!! &&&&&&&&&&
            if (select[i].equals(answ.getAnswer(i))) {
                score = score + 1;
            }
        }  */
        //score = score * 2;
        terminar.setEnabled(false);
        regresar.setEnabled(false);
        btExit.setEnabled(true);
        System.exit(0);
        //  WindowScore sc = new WindowScore(score);
        // sc.setVisible(true);
        // sc.setLocationRelativeTo(null);
    }//GEN-LAST:event_terminarActionPerformed


    private void regresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regresarActionPerformed
        if (posicion > 0) {
            posicion--;
        }

        if (posicion == 0) {
            regresar.setEnabled(false);
            btExit.setEnabled(true);
        }

        if (posicion > -1) {
            avanzar.setEnabled(true);
            btExit.setEnabled(true);
            try {
                textAndQuestion = quest.getTextQuestionsDB(posicion);
            } catch (IOException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ExamInterface.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (getText(textAndQuestion).equals(" ")) {
                jTextField1.setText(getQuestion(textAndQuestion));
            } else {
                jTextField1.setText(getText(textAndQuestion) + "\n\n"
                        + getQuestion(textAndQuestion));
            }

            String[] a = answ.setAnswers(posicion);
            buttonGroup1.clearSelection();
            opc1.setText(a[0]);
            opc2.setText(a[1]);
            opc3.setText(a[2]);
            opc4.setText(a[3]);
            opc1.requestFocus();
        } else {
            Toolkit.getDefaultToolkit().beep();
        }

    }//GEN-LAST:event_regresarActionPerformed

    private void btExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btExitActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton avanzar;
    private javax.swing.JButton btExit;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextField1;
    private javax.swing.JRadioButton opc1;
    private javax.swing.JRadioButton opc2;
    private javax.swing.JRadioButton opc3;
    private javax.swing.JRadioButton opc4;
    private javax.swing.JButton regresar;
    private javax.swing.JButton terminar;
    // End of variables declaration//GEN-END:variables
}
