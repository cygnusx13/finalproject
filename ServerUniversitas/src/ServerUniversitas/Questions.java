package ServerUniversitas;

import com.sun.rowset.CachedRowSetImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Platon
 */
public class Questions {

    Client cliente = new Client();
    // DataBase db = new DataBase();
    public CachedRowSetImpl crs = null;
    ArrayList<String> questions = null;
    int quantityRows = 0;

    public Questions() {
        //this.crs = crs;
    }

    public String getTextQuestionsDB(int posicion) throws IOException, ClassNotFoundException, SQLException {
        crs = cliente.getDataBase();
        questions = new ArrayList<>();
        //crs = db.DataBases();

        try {
            while (crs.next()) {
                String text;
                text = crs.getString(2);
                if (text.length() == 0) {
                    text = " ";
                }
                String question = crs.getString(3);
                text += "|";
                text += question;
                questions.add(text);
          /*             System.out.println(crs.getString("texto"));
                System.out.println(crs.getString("question"));
                System.out.println(crs.getString("opc1"));
                System.out.println(crs.getString("opc2"));
                System.out.println(crs.getString("opc3"));
                System.out.println(crs.getString("opc4"));
                System.out.println(crs.getString("answer")); 
                //System.out.println(crs.getString("image"));  */
            }
        } catch (SQLException ex) {
            Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex);
        }
        quantityRows = questions.size() + 1;
        return questions.get(posicion);
    }

    public int quantityRowsDB() {
       // System.out.println("quantityRows = " +quantityRows );
        return quantityRows;
    }

    /*
    public String getTextQuestions(int posicion) throws SQLException {
        ArrayList<String> questions2 = getTextQuestionsDB();

        return questions2.get(posicion);
    }
     */
 /*  String[] questions = {
        "Pregunta 1?", "Pregunta 2?", "Pregunta 3?", "Pregunta 4?",
        "Pregunta 5?"
    };
     */
 /*
    String [] questions = new String[5];
    
    public void instansQuestions() {
        try {
            while (crs.next()) {

                for (int i = 0; i < 10; i++) {
                    questions [i] = crs.getString("question");
                }

//System.out.println(crs.getString("question"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getTextQuestionsDB(int posicion) {
        return questions[posicion];
    }

     */
}
