package ServerUniversitas;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sun.rowset.CachedRowSetImpl;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

/**
 *
 * @author Wilson Castro. 2017
 */
class DataBase implements Serializable {

    final String DRIVER = "com.mysql.jdbc.Driver";
    final String CONNECTION = "jdbc:mysql://localhost:3306/testdb";  // localhost:3306
    Properties p = new Properties();
    private String answer = null;
    private CachedRowSetImpl crs = null;

    public CachedRowSetImpl DataBases() {
        p.put("user", "testdb");
        p.put("password", "testdb");
        ResultSet resultDeTabla = null;

        try {
            Class.forName(DRIVER).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = (Connection) DriverManager.getConnection(CONNECTION, p);
                Statement statement = (Statement) connection.createStatement()) {
            System.out.println("Conected to DB");

            resultDeTabla = statement.executeQuery("select * from lecturacritica");

            /*    while (resultDeTabla.next()) {
                System.out.println(resultDeTabla.getString("Mes") + ", " + resultDeTabla.getString("Estacion"));
            } 
             */
            crs = new CachedRowSetImpl();
            crs.populate(resultDeTabla);
            connection.close();
            /* while (crs.next()) {
                    System.out.println(crs.getString("Mes") + ", " + crs.getString("Estacion"));
                }
             */
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return crs;
    }
}
