package ServerUniversitas;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wilson Castro. 2017
 */
class ServerThreaded implements Runnable {

    BufferedReader in = null;
    PrintWriter out = null;
    private Socket client = null;
    BufferedOutputStream bOutStr = null;
    PrintWriter outObj = null;
    ObjectInputStream objInStrm = null;
    ObjectOutputStream objOutStr = null;

    public ServerThreaded(Socket client) {
        this.client = client;
    }

    public void run() {
        DataBase db = null;

        try {
            in = new BufferedReader(
                    new InputStreamReader(client.getInputStream()));
            //out = new PrintWriter(new BufferedOutputStream(client.getOutputStream()), true);

            //outObj = new PrintWriter(new BufferedOutputStream(client.getOutputStream()), true);
            //objInStrm = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            objOutStr = new ObjectOutputStream(client.getOutputStream());

            String input;
            System.out.println("Ожидаем сообщений");

            while ((input = in.readLine()) != null) {

                if (input.equals("base")) {
                    db = new DataBase();
                    objOutStr.writeObject(db.DataBases());
                    objOutStr.flush();
                    continue;
                }
                if (input.equalsIgnoreCase("exit")) {
                    break;
                }
                System.out.println(input);

            }
            // out.close();
            objOutStr.close();
            in.close();
            client.close();
            //objInStrm.close();
            // outObj.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class ConnectionStart {

    private Socket client = null;
    private int port;

    public ConnectionStart() {
        port = 1234;
    }

    public void startConnection() {

        System.out.println("Старт сервера");

        ServerSocket server = null;
        try {
            int i = 1;
            server = new ServerSocket(port);
            while (true) {
                client = server.accept();
                System.out.println("Conection № " + i);
                Runnable r = new ServerThreaded(client);
                Thread t = new Thread(r);
                t.start();
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
