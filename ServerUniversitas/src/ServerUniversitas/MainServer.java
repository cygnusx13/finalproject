package ServerUniversitas;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Wilson Castro. 2017
 */
public class MainServer {

    public static void main(String[] args) throws IOException {
        ConnectionStart star = new ConnectionStart();
        star.startConnection();
        DataBase db= new DataBase();
        db.DataBases();
    }
}
