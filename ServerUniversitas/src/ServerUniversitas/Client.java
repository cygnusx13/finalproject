package ServerUniversitas;

import com.sun.rowset.CachedRowSetImpl;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Blob;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

/**
 * @author Platon
 */
public class Client {
    // PrintWriter outObj = null;

    ObjectInputStream objInStrm = null;
    ObjectOutputStream objOutStr = null;
    private String ip;
    public CachedRowSetImpl crs = null;
    // Questions qs = null;

    public Client() {
        this.ip = "localhost";
    }

    public CachedRowSetImpl connectClient() throws IOException, ClassNotFoundException, SQLException {

        System.out.println("Клиент стартовал");
        Socket server = null;

        if (ip == null) {
            System.out.println("Использование: java Client IP"); // 192.168.1.36
            System.exit(-1);
        }

        System.out.println("Соединяемся с сервером " + ip);

        server = new Socket(ip, 1234);

        //  BufferedReader in  = new BufferedReader(
        //         new  InputStreamReader(server.getInputStream()));
        PrintWriter out
                = new PrintWriter(server.getOutputStream(), true);

        BufferedReader inu
                = new BufferedReader(new InputStreamReader(System.in));
        objInStrm = new ObjectInputStream(server.getInputStream());
        //outObj = new PrintWriter(new BufferedOutputStream(server.getOutputStream()), true);

        String fuser, fserver, dataserver;

        while (true) {
            fuser = "base"; //inu.readLine();
            out.println(fuser); //envia info (lo q se digita o otra cosa) al servidor
            // fserver = in.readLine(); // lee el eco que devuelve el servidor

            if (fuser.equals("base")) {
                try {
                    crs = (CachedRowSetImpl) objInStrm.readObject();
                    //qs = new Questions();  //&&&&&&&&&&&&
                    while (crs.next()) {

                        // System.out.println(crs.getString("Mes") + ", " + crs.getString("Estacion") );
                        //Blob blob = crs.getBlob("image");
                        //continue;
                        //System.out.println(crs.getInt("id") + ", " + crs.getString("question") + ", " + crs.getString("opc1")  + ", " + crs.getString("opc2")  + ", " + crs.getString("opc3")  + ", " + crs.getString("opc4") + ", " + crs.getString("answer") );
                        //System.out.println(crs.getInt("id") + ", " + crs.getString("question") + ", " + crs.getString("opc1") + ", " + crs.getString("opc2") + ", " + crs.getString("opc3"));
                            /*       System.out.println(crs.getString("texto"));
                        System.out.println(crs.getString("question"));
                        System.out.println(crs.getString("opc1"));
                        System.out.println(crs.getString("opc2"));
                        System.out.println(crs.getString("opc3"));
                        System.out.println(crs.getString("opc4"));
                        System.out.println(crs.getString("answer"));
                        //System.out.println(crs.getString("image")); */
                        return crs;
                    }
                } catch (Exception e) {
                    System.out.println("Excepcion al deserializar: " + e);
                    System.exit(0);
                }
            }

            //out.flush();
            // server.close();
            objInStrm.close();
            out.close();
            server.close();

        }
        // in.close();
        //inu.close();
        //outObj.close();
    }

    public CachedRowSetImpl getDataBase() throws IOException, ClassNotFoundException, SQLException {
        crs = connectClient();
        return crs;
    }
}
