
package ServerUniversitas;

import java.util.StringTokenizer;

/**
 * @author Platon
 */
public class Answers {
     String[] answers = {"1", "2", "3", "4", "5"};

    String[] radioR = {
        "1, 2, 3, 4",
        "2, 3, 4, 5",
        "3, 4, 5, 6",
        "4, 5, 6, 7",
        "5, 6, 7, 8"
    };

    public String getAnswer(int position) {
        return answers[position];
    }

    public String[] separate(String cadena, String separator) {
        StringTokenizer token = new StringTokenizer(cadena, separator);

        String[] a = new String[4];
        int i = 0;

        while (token.hasMoreTokens()) {
            a[i] = token.nextToken();
            i++;
        }
        return a;
    }

    public String[] setAnswers(int position) {
        String s1 = radioR[position];
        String[] s2 = separate(s1, ",");
        return s2;
    }
}
