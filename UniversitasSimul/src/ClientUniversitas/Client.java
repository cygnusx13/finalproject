package ClientUniversitas;

import java.io.*;
import java.net.*;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

/**
 *
 * @author Wilson Castro. 2017
 */
class Client {

    PrintWriter outObj = null;
    ObjectInputStream objInStrm = null;
    ObjectOutputStream objOutStr = null;
    private String ip;

    public Client(String ip) {
        this.ip = ip;
    }

    public void connectClient() throws IOException, ClassNotFoundException, SQLException {

        System.out.println("Клиент стартовал");
        Socket server = null;

        if (ip == null) {
            System.out.println("Использование: java Client IP"); // 192.168.1.36
            System.exit(-1);
        }

        System.out.println("Соединяемся с сервером " + ip);

        server = new Socket(ip, 1234);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(server.getInputStream()));
        PrintWriter out
                = new PrintWriter(server.getOutputStream(), true);
        BufferedReader inu
                = new BufferedReader(new InputStreamReader(System.in));

        objInStrm = new ObjectInputStream(new BufferedInputStream(server.getInputStream()));
        outObj = new PrintWriter(new BufferedOutputStream(server.getOutputStream()), true);

        String fuser, fserver, dataserver;

        while ((fuser = inu.readLine()) != null) {
            out.println(fuser); //envia info (lo q se digita o otra cosa) al servidor
            fserver = in.readLine(); // lee el eco que devuelve el servidor

            System.out.println(fserver); //muestra eco del servidor en la consola

            /* while (dataserver!=null) {
                //System.out.println(fserver.getString("Mes") + ", " + resultDeTabla.getString("Estacion"));
                System.out.println(dataserver);
            }
             */
            if (fuser.equalsIgnoreCase("close")) {
                break;
            }
            if (fuser.equalsIgnoreCase("exit")) {
                break;
            }
            if (fuser.equals("base")) {
                try {

                    CachedRowSet crs = (CachedRowSet) objInStrm.readObject();

                    while (crs.next()) {
                        System.out.println(crs.getString("Mes") + ", " + crs.getString("Estacion"));
                    }
                } catch (Exception e) {
                    System.out.println("Excepcion al deserializar: " + e);
                    System.exit(0);
                }
            }
        }
        out.close();

        in.close();

        inu.close();

        server.close();
    }
}
