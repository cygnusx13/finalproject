
package ClientUniversitas;

import java.io.IOException;

/**
 *
 * @author Wilson Castro. 2017
 */
public class MainClient {
    
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Client cl= new Client("localhost");
        cl.connectClient();
        
    }
}
